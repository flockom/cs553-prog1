#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include <errno.h>

/*read/write*/
#include <unistd.h> 

/*network*/
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define DEBUG 0

#if DEBUG == 1
  #define DPRINTF(args) printf args
#else
  #define DPRINTF(args) 
#endif


/*TDOD:* /

/*
  USAGE: $ netmark-srv PORT 
  PORT - port to listen on for incoming requests ex. "1234"
*/

typedef struct {/*fixed size, since its coming over the network*/
  int32_t fd;
  int32_t size;  
} worker_args;

void error(char * meg);
void *tcp_worker(void *args);
void *udp_worker(void *args);

int main(int argc, char** argv){
  DPRINTF(("starting up...\n"));
  fflush(NULL);
  uint16_t port = (uint16_t)atoi(argv[1]);


  struct sockaddr_in serv_addr,client_addr;
  memset((void*)&serv_addr,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  

  //create socket
  int sockfd = socket(AF_INET,SOCK_STREAM,0);
  if(sockfd<0)
    error("error creating socket");
  
  DPRINTF(("socket created...\n"));
  fflush(NULL);
  
  /*don't have to wait to rebind port after restart*/
  int option = 1;
  setsockopt(sockfd,SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

  //bind it
  if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
    error("error binding socket");

  DPRINTF(("socket bound...\n"));
    

    //listen for connections
    //second argument is/proc/sys/net/core/somaxconn 
    if(listen(sockfd,128)<0)
      error("error listening on socket");

    DPRINTF(("listening for connections...\n"));
  
    int *connfd;  
    pthread_t thread;
    //spin a new thread for each connection  
    while(1){
      worker_args *worker = malloc(sizeof(worker_args));
      if((worker->fd = accept(sockfd,NULL,NULL)) <0)
	error("error accepting connection");
      DPRINTF(("connection accepted %d...spinning up thread\n",*connfd));
      /*get the size and protocol this client would like to use*/
      char info[8];/*always good variable names*/
      if(read(worker->fd,info,8)!=8)/*lets just hope it all comes through*/
	error("error getting size and protocol form client");
      int32_t protocol= *(int32_t*)info;
      worker->size = *(int32_t*)(info+4);      
      pthread_create(&thread,NULL,(protocol)?(udp_worker):(tcp_worker),(void*)worker);
    }
  
}

void *tcp_worker(void *args){
  worker_args *a = (worker_args*)args;
  int size = a->size;  
  int fd = a->fd;
  char in[size];
  free(args);
  int n,nn;

  while(1){
    //read the data from the socket
    DPRINTF(("waiting for %d bytes on %d\n",size,fd));
    n = size;
    while(n){//make sure we read the full ammount
      DPRINTF(("attempting to read %d bytes\n",n));
      nn = read(fd,in+size-n,n);
      if(nn<0)
	error("error reading from client");
      if(nn == 0){
	DPRINTF(("connection closed by client\n"));
	if(close(fd)<0)
	  error("error closing socket");
	return NULL;
      }
      n -=nn;
    }  
    DPRINTF(("writing response\n"));
    //write back response, one byte
    if(write(fd,in,1)<0)
      error("error writing response to client");
  }  
  return NULL;
}

void *udp_worker(void *args){
  worker_args *a = (worker_args*)args;
  int size = a->size;  
  int fd = a->fd;
  char in[size];
  //bind a udp socket
  int sfd = socket(AF_INET,SOCK_DGRAM,0);
  if(sfd<0)
    error("error creating socket in udp_worker");

  struct sockaddr_in serv_addr;
  memset((void*)&serv_addr,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = 0;/*take any port*/
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  int slength = sizeof(struct sockaddr_in);

  DPRINTF(("binding udp socket to a random port...\n"));

  if(bind(sfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
    error("error binding socket in udp_worker");

  //get the port we just bound to
  memset((void*)&serv_addr,0,sizeof(serv_addr));/*maybe this is necessary?*/
  if(getsockname(sfd,(struct sockaddr*)&serv_addr,&slength)<0)
    error("error getting socket name in udp_worker");
    
  DPRINTF(("bound server to udp port: %d\n",serv_addr.sin_port));


  DPRINTF(("sending new port to client...\n"));
  //tell the client about it
  if(write(fd,(void*)&serv_addr,slength)!=slength) /*just assume we write it all*/
    error("error sending new port to client in udp_worker");

  //close the tcp socket
  if(close(fd)<0)
    error("error closing tcp socket in udp_worker");
  

  DPRINTF(("beginning request/response loop\n"));
  //request-response loop
  struct sockaddr_in client_addr;
  int m;
  uint32_t n=0;

  struct timeval tv;
  tv.tv_sec = 3; /*3 sec timeout*/
  setsockopt(sfd,SOL_SOCKET,SO_RCVTIMEO,(void*)&tv,sizeof(tv));
  int finished=0;
  while(!finished){
    /* wait for some data*/
    DPRINTF(("waiting for some data...\n"));    
    if((m=recvfrom(sfd,in,size,0,(struct sockaddr*)&client_addr,&slength)) != size){
      if(m>0)
	error("read wrong ammount of data, didn't know this could happen w/ udp");
      if(m == 0 || errno == EAGAIN || errno == EWOULDBLOCK){
	DPRINTF(("connection closed or 3 sec timeout, closing down %d\n",m));	
	finished =1;
      }else      
	error("error receiving from udp socket");   
    }
    if(!finished){
      n++;/*we got a packet*/
      DPRINTF(("got packet #%d\n",n));    
      //tell the client how many packets we have so far
      sendto(sfd,(void*)&n,sizeof(n),0,(struct sockaddr*)&client_addr,slength);
    }
  }    
  
  DPRINTF(("thread finished\n"));	
  close(sfd);
  free(args);
  return NULL;
}


void error(char * msg){
  perror(msg);
  exit(0);
}
