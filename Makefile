cpumark: cpumark.c
	gcc -std=gnu99 -lpthread cpumark.c -o cpumark

netmark-srv: netmark-srv.c
	gcc -std=gnu99 -lpthread netmark-srv.c -o netmark-srv

netmark-cli: netmark-cli.c
	gcc -std=gnu99 -lpthread netmark-cli.c -o netmark-cli

storemark: storemark.c
	gcc -g -std=gnu99 -lpthread storemark.c -o storemark

.PHONY:
clean:
	rm -f cpumark netmark-cli netmark-srv  storemark *~