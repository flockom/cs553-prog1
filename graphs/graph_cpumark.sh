#!/bin/bash

# USAGE: ./graph_cpumark.sh TYPE NUM OUT
#   TYPE   - 1 for FLOPS, 0 for IOPS
#   NUM    - number of giga operations to perform
#   OUT    - prefix of output, OUT.dat contains raw data, OUT.png is graph


# we need to run the benchmark four times for 1,2,4,8 threads


TYPE=$1
NUM=$2
OUT=$3

if [ "$TYPE" = "1" ]; then
    OP="FLOPs"
else
    OP="IOPs"
fi

rm -f $OUT.dat
echo -e "#threads\t giga $OP" > $OUT.dat

# generate the data file
for i in 1 
do
    echo -n "working on $i threads..."

    echo -ne "$i\t\t" >> $OUT.dat
    ./cpumark $TYPE $NUM $i >> $OUT.dat
    echo "" >> $OUT.dat    

    echo "done."
done



#generate the plot

echo -e "
set terminal png
set output '$OUT.png'

set title 'giga $OP at different levels of concurrency'
set nokey

set xlabel 'number of threads'
set ylabel 'giga $OP'

set xtics (1,2,4,8)

set style data linespoints

plot '$OUT.dat' using 1:2
" > $OUT.gp

gnuplot $OUT.gp

