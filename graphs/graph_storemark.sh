#!/bin/bash

# USAGE: ./graph_storemark.sh MEDIA OUT
#   MEDIA   - 1 for ram, 0 for disk
#   DUR     - number of seconds for each data point
#   SIZE    - Total size(MB) to operate on(divided amongst all threads)
#   OUT     - prefix of output, OUT.png is graph 
#             OUT-sw.dat contains raw data for sequential reads
#             OUT-rw.dat for random writes
#             OUT-sr.dat for sequential reads
#             OUT-rr.dat for random reads


# SIZE must be > 8000 for disk so each thread has a full gig on 8 threads


# we need to run the benchmark four times for 1,2,4,8 threads




MEDIA=$1
DUR=$2
SIZE=$3
OUT=$4
#BLKS=${@:4:$#}


if [ "$MEDIA" = "1" ]; then
    OP="RAM"
#    BLKS=( 1 10  100  1000 10000 100000 1000000 )
#    HBLKS=( 1B 10B 100B 1KB  10KB  100KB  1MB )
    BLKS=( 1 100 10000 )
    HBLKS=( 1B 100B 10KB )
else
    OP="Disk"
    BLKS=( 1 10000 1000000 100000000 )
    HBLKS=( 1B 10KB  1MB  100MB   )
#    BLKS=( 1 10 100 10000 )
#    HBLKS=( 1B 10B 100B 10KB )
fi


rm -f $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat

#print the header to each output file
echo -n "#threads " | tee $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat > /dev/null
for i in "${HBLKS[@]}"; do
    echo -n "$i " | tee -a $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat > /dev/null
done
echo "" | tee -a $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat > /dev/null


#run the thing, the output is for gnuplot in column format as:
#threads  bandwidth_blksize#1 latency_blksize#1 ...bandwidth_blksize#n latency_blksize#n
for THREADS in 1;do
    echo "running for $THREADS threads"
    
    TSIZE=$SIZE
    #create the files if we are doing disk
    if [ "$MEDIA" = "0"  ];then
	TSIZE=( ) # unset incase thread sizes are not increasing(will get old array elements)
	TSIZE=$(($SIZE/$THREADS)) #per-thread size
	ii=0
	while [ $ii -lt $THREADS ];do
	    TMP[$ii]=`mktemp`
	    echo "dd if=/dev/zero of=${TMP[$ii]} bs=1000000 count=$((TSIZE))"
	    dd if=/dev/zero of=${TMP[$ii]} bs=1000000 count=$((TSIZE))
	    let ii=ii+1
	done
	TSIZE="${TMP[@]}"
    fi
    i=0
    #print the thread number
    echo -n "$THREADS " | tee -a $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat > /dev/null
    while [ $i -lt ${#BLKS[@]} ];do
	echo -e "\trunning for ${HBLKS[$i]}"
	echo "./storemark $MEDIA ${BLKS[$i]} $THREADS $DUR $TSIZE"
	RES=`./storemark $MEDIA ${BLKS[$i]} $THREADS $DUR $TSIZE`
	echo $RES
	#print the throughput and latency to each file without a newline
	echo -n $RES | cut -d " " -f 1-2 | tr "\n" " " >> $OUT-sw.dat
	echo -n $RES | cut -d " " -f 3-4 | tr "\n" " " >> $OUT-rw.dat
	echo -n $RES | cut -d " " -f 5-6 | tr "\n" " " >> $OUT-sr.dat
	echo -n $RES | cut -d " " -f 7-8 | tr "\n" " " >> $OUT-rr.dat
		
	let i=i+1
    done
    #print the newline
    echo "" | tee -a $OUT-sw.dat $OUT-rw.dat $OUT-sr.dat $OUT-rr.dat > /dev/null
    #delete the tmp files if were doing disk
    if [ "$MEDIA" = "0"  ];then
	for F in ${TMP[@]};do
	    rm -f $F
	done	
    fi
done


#USAGE: plot TITLE OUT 
function plot_bandwidth {

#build the plot command
j=1
PLOT="plot '$2.dat' using 1:2 title '${HBLKS[0]}'"
while [ $j -lt ${#HBLKS[@]} ];do  
    let COL=(j+1)*2
    PLOT=$PLOT",'$2.dat' using 1:$COL title '${HBLKS[$j]}'"
    let j=j+1
done
echo $PLOT

echo -e "
set terminal png
set output '$2.png'

set title '$1'
set key outside bottom

set xlabel 'number of threads'
set ylabel 'MB/s'

set xtics (1,2,4,8)

set style data linespoints

$PLOT
" > $OUT.gp
gnuplot $OUT.gp    
}

#build the figures
plot_bandwidth "Sequential writes for different block sizes at varying levels of concurrency" $OUT-sw
plot_bandwidth "Random writes for different block sizes at varying levels of concurrency" $OUT-rw
plot_bandwidth "Sequential reads for different block sizes at varying levels of concurrency" $OUT-sr
plot_bandwidth "Random reads for different block sizes at varying levels of concurrency" $OUT-rr



