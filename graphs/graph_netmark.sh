#!/bin/bash

# USAGE: ./graph_netmark.sh PROTOCOL SERVER PORT DURATION OUT
#   PROTOCOL     - 0 for tcp, 1 for udp
#   SERVER       -  ip where server is running
#   PORT         -  port which server is bound to
#   DURATION     -  duration to send requests for each block size
#   OUT          - prefix of output, OUT.png is graph 

# we need to run the benchmark four times for 1,2,4,8 threads



PROTOCOL=$1
SERVER=$2
PORT=$3
NUM_REQ=$4
OUT=$5

if [ $PROTOCOL -eq 1 ];then
    HPROTOCOL="UDP"
    BLKS=( 1 100 10000 60000)
    HBLKS=( 1B 100B 10KB 60KB )
else
    HPROTOCOL="TCP"
    BLKS=( 1   100 10000  1000000 )
    HBLKS=( 1B 100B 10KB 1MB )
fi




rm -f $OUT.dat

#print the header
echo -n "#threads " > $OUT.dat
for i in "${HBLKS[@]}"; do
    echo -n "$i " >> $OUT.dat
done
echo "" >> $OUT.dat

#run it for each block size
for THREADS in 1 ; do
    echo "running for $THREADS threads"
    i=0
    #print the thread number
    echo -n "$THREADS " >> $OUT.dat
    while [ $i -lt ${#BLKS[@]} ];do
	echo -e "\trunning for ${HBLKS[$i]}"
	RES=`./netmark-cli ${BLKS[$i]} $NUM_REQ $THREADS $PROTOCOL $SERVER $PORT`
	echo -e "\t" $RES
	#print the throughput and latency
	echo -n $RES" " >> $OUT.dat
		
	let i=i+1
    done
    #print the newline
    echo "" >> $OUT.dat
done


#USAGE: plot TITLE OUT 
function plot_bandwidth {

#build the plot command
j=1
PLOT="plot '$2.dat' using 1:2 title '${HBLKS[0]}'"
while [ $j -lt ${#HBLKS[@]} ];do  
    let COL=(j+1)*2
    PLOT=$PLOT",'$2.dat' using 1:$COL title '${HBLKS[$j]}'"
    let j=j+1
done
echo $PLOT

echo -e "
set terminal png
set output '$2.png'

set title '$1'
set key outside bottom

set xlabel 'number of threads'
set ylabel 'Mb/s'

set xtics (1,2,4,8)

set style data linespoints

$PLOT
" > $OUT.gp
gnuplot $OUT.gp    
}


plot_bandwidth "Throughput of $HPROTOCOL for different request sizes at varying levels of concurrency" $OUT