#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>

//#define FLOP acc[0]+=0.001;acc[1]+=0.001;acc[2]+=0.001;acc[3]+=0.001; \
//  acc[4]+=0.001;acc[5]+=0.001;acc[6]+=0.001;acc[7]+=0.001;
#define FLOP acc0+=acc0;mul0*=mul0;\
  acc1+=acc1;mul1*=mul1;						\
  acc2+=acc2;mul2*=mul2;						\
  acc3+=acc3;mul3*=mul3;						\
  acc4+=acc4;mul4*=mul4;						\
  acc5+=acc5;mul5*=mul5;						\
  acc6+=acc6;mul6*=mul6;						\
  acc7+=acc7;mul7*=mul7;						\
  acc8+=acc8;mul8*=mul8;						\
  acc9+=acc9;mul9*=mul9;						\
  acc10+=acc10;mul10*=mul10;						\
  acc11+=acc11;mul11*=mul11;
#define FLOPC 24
#define IOPC 24

#define IOP FLOP
#define UNROLL 6
#define GIGA 1000000000

void* speedF(void* nump);
void* speedI(void* nump);

/*
  TODO: IOPS support
*/

/*USAGE:
  $ cpumark TYPE NUM THREADS
  
  TYPE    - 1 for FLOPS, 0 for IOPS
  NUM     - Number of giga operations to perform
  THREADS - Number of threads to use, each thread does NUM/THREADS ops

  OUTPUT: number of giga OPS
  
*/
int main(int argc, char** argv){
  int type = atoi(argv[1]);
  int num  = atol(argv[2]);  
  int num_threads  = atoi(argv[3]);

  uint64_t num_per_thread = (num/num_threads);

  //printf("Running %ld giga-%s with %d threads\n",num,(type)?"FLOP":"IOP",num_threads);


  time_t start = time(NULL);

  void* (*op)(void*) = (type)? (speedF):(speedI);

  /*spin up threads for the appropriate op type*/
  pthread_t thread[num_threads];
  for(int i=0;i<num_threads;i++){
    pthread_create(&(thread[i]),NULL,op,(void*)(&num_per_thread));
  }   

  //wait for all threads to finish their work
  for(int i=0;i<num_threads;i++){
    pthread_join(thread[i],NULL);
  }   

  //calculate gigaOPS
  float gops = num*1.0/((time(NULL)-start));
  printf("%f",gops);
  
}

void* speedF(void* nump){
  uint64_t num = *(uint64_t*)nump;

  float acc0,acc1,acc2,acc3,acc4,acc5,acc6,acc7,acc8,acc9,acc10,acc11;
  acc0=acc1=acc2=acc3=acc4=acc5=acc6=acc7=acc8=acc9=acc10=acc11 = 0.01;
  float mul0,mul1,mul2,mul3,mul4,mul5,mul6,mul7,mul8,mul9,mul10,mul11;
  mul0=mul1=mul2=mul3=mul4=mul5=mul6=mul7=mul8=mul9=mul10=mul11 = 1.1;
  
  num = num*GIGA/(UNROLL*FLOPC);

  for(uint64_t i=0;i<num;i++){
    FLOP;
    FLOP;
    FLOP;
    FLOP;
    FLOP;
    FLOP;
  }
  return NULL;
}



void* speedI(void* nump){
  uint64_t num = *(uint64_t*)nump;

  int acc0,acc1,acc2,acc3,acc4,acc5,acc6,acc7,acc8,acc9,acc10,acc11;
  int mul0,mul1,mul2,mul3,mul4,mul5,mul6,mul7,mul8,mul9,mul10,mul11;
  acc0=acc1=acc2=acc3=acc4=acc5=acc6=acc7=acc8=acc9=acc10=acc11 = 1;
  mul0=mul1=mul2=mul3=mul4=mul5=mul6=mul7=mul8=mul9=mul10=mul11 = 3;
  
  num = num*GIGA/(UNROLL*IOPC);

  for(uint64_t i=0;i<num;i++){
    IOP;
    IOP;
    IOP;
    IOP;
    IOP;
    IOP;
  }
  return NULL;
}
