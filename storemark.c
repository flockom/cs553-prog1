#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include <unistd.h>
#include <sys/stat.h>

#define DEBUG 0

#if DEBUG == 1
  #define DPRINTF(args) printf args
#else
  #define DPRINTF(args) 
#endif

void error(char * msg);
void print_bw_latency(time_t duration,long total_ops,long blk_size);

void *disk_read(void*);
void *disk_write(void*);
void *ram_read(void*);
void *ram_write(void*);

typedef struct {
  int fd;            /*used for disk OPs*/
  char path[50];      /*store filename for deletion*/
  
  void *buf;         /*used for memory OPs*/

  int blk_size;      /*size of each OP*/ 
  long tot_size;      /*size of file or memory area*/
  int pattern;       /*0-sequential, 1-random*/
  int duration;     /*number of seconds to operate for*/
} worker_args;


/*
  USAGE: $ storemark MEDIA BLKSIZE NUMTHREADS DURATION SIZE/[FILES]
  
  MEDIA      - 0-disk , 1-ram
  BLKSIZE    - size of each OP (bytes)
  NUMTHREADS - number of threads to use (each is independent)
  DURATION   - number of seconds to perform each for the 4 operations
  [FILES]    - names of files to use, must be one for each thread, they should already be created
             - their size will effect the throughput of random ops. ONLY IF MEDIA=0
  SIZE       - number of MB of ram to operate on, divided amongst all threads. ONLY IF MEDIA=1

  OUTPUT:
  sequentail write (t)hroughput  (MB/s)
  sequential write avg (l)atency (ms/request)
  random write t
  random write l  
  sequential read t
  sequential write l
  random read t
  random read l
*/
int main(int argc, char** argv){
  DPRINTF(("starting up, parsing args...\n"));
  /*get args*/
  int media = atoi(argv[1]);
  long blk_size  = atol(argv[2]);
  int num_threads = atoi(argv[3]);
  /*multiple of blk_size*/
  int duration = atoi(argv[4]);
  
  

  DPRINTF(("seeding random..."));
  /*
    initi random # generator, seed w/ time , we want it to be
    different across runs.  don't really need reproducable values so
    not using reentrant versions.
  */
  srandom((unsigned int)time(NULL));
  DPRINTF(("done\n"));

  

  DPRINTF(("malloc storage/open files..."));
  /*malloc ram or open files*/
  worker_args args[num_threads];
  for(int i=0;i<num_threads;i++){
    args[i].blk_size = blk_size;
    args[i].duration = duration;
    if(media){ /*ram - malloc the data*/
      args[i].tot_size = atoi(argv[5])*1000000/num_threads;
      if((args[i].buf = malloc(args[i].tot_size)) == NULL)
	error("error doing malloc");
    }else{     /*disk - open a file*/
      /*open the file passed in*/
      strncpy(args[i].path,argv[5+i],50);
      if((args[i].fd = open(args[i].path,O_RDWR)) <0)
	error("error opening file");
      /*get its size*/
      struct stat buf;
      fstat(args[i].fd,&buf);
      args[i].tot_size = buf.st_size;
    }
  }
  DPRINTF(("done\n"));

  void * (*f[2]) (void*);
  if (media){
    f[0] = ram_write;
    f[1] = ram_read;    
  }
  else{
    f[0] = disk_write;
    f[1] = disk_read;
  }

  
  /*sequential writes*/
  /*random writes*/
  /*sequential reads*/  
  /*random reads*/
  pthread_t thread[num_threads];
  for(int i=0;i<4;i++){

    DPRINTF(("\n\nspinning up threads\n"));
    /*low bit is pattern 0-sequential, 1-random*/
    for(int ii=0;ii<num_threads;ii++)
      args[ii].pattern = i & 0x1;        

    //start the clock
    time_t start = time(NULL);

    //spin up all the threads
    for(int ii=0;ii<num_threads;ii++){
      /*high bit is the operation, 0-write, 1-read*/
      pthread_create(&thread[ii],NULL,f[(i&0x2)>>1 ],(void*)&args[ii]);
    }   

    DPRINTF(("waiting for threads to finish\n"));
    unsigned long num_requests=0;
    uint64_t *ct;
    //wait for them to finish their work, add up total requests
    for(int ii=0;ii<num_threads;ii++){
      pthread_join(thread[ii],(void**)&ct);
      num_requests += *ct;
      free(ct);
    }
    
    DPRINTF(("done\n"));
    //stop the clock, print results
    print_bw_latency(time(NULL)-start,num_requests,blk_size);
  }
  
  //clean up, dont delete the file it is to be re-used
  for(int i=0;i<num_threads;i++){
    if(media){
      free(args[i].buf);
    }else{
      if(close(args[i].fd)<0)
	error("error closing file");      
    }
  }

}


void *disk_read(void *args){
  DPRINTF(("disk read\n"));
  worker_args * a = (worker_args *)args;  
  uint64_t* count=malloc(sizeof(uint64_t));
  *count = 0;
  uint64_t n,nn;
  void *dst = malloc(a->blk_size);

  if(lseek(a->fd,0,SEEK_SET)<0)
    error("error seeking in file");

  time_t start = time(NULL);
  while(time(NULL)-start < a->duration){
    /*for random access, first choose a random location and seek to it*/
    if(a->pattern){
      double per = rand()*1.0/RAND_MAX;
      off_t offset = (off_t)(per*1.0*(a->tot_size-a->blk_size-1));
      //DPRINTF(("seeking to %lu (%f\%) \n",offset,offset*100.0/(a->tot_size)));
      if(lseek(a->fd,offset,SEEK_SET)<0)
	error("error seeking in file");
    }else{/*if we are at the end of the file seek back to beginning*/
      if((*count)*a->blk_size > a->tot_size-a->blk_size-1){
	if(lseek(a->fd,0,SEEK_SET)<0)
	  error("error seeking to front of file");
      }	
    }
    
    //DPRINTF(("reading block %d:\n",*count));
    n = a->blk_size;
    while(n){//make sure we read the full ammount
      nn = read(a->fd,dst+a->blk_size-n,n);
      //DPRINTF(("\t read %d bytes\n",nn));
      if(nn<0)
	error("error reading file");
      n -=nn;
    }
    (*count)++;
  }
  DPRINTF(("%d reads\n",*count));
  free(dst);
  pthread_exit((void*)count);
  return NULL;
}


void *disk_write(void *args){

  DPRINTF(("disk write\n"));
  worker_args * a = (worker_args *)args;  
  uint64_t n,nn;
  void *src = malloc(a->blk_size); 
  memset(src,'p',a->blk_size);
  
  if(lseek(a->fd,0,SEEK_SET)<0)
    error("error seeking in file");

  uint64_t* count=malloc(sizeof(uint64_t));
  *count = 0;
  time_t start = time(NULL);

  while(time(NULL)-start < a->duration){
    /*for random access, first choose a random location and seek to it*/
    if(a->pattern){
      double per = rand()*1.0/RAND_MAX;
      off_t offset = (off_t)(per*1.0*(a->tot_size-a->blk_size-1));
      //DPRINTF(("seeking to %lu (%f\%) \n",offset,offset*100.0/a->tot_size));
      if(lseek(a->fd,offset,SEEK_SET)<0)
	error("error seeking in file");
    }else{/*if we are at the end of the file seek back to beginning*/
      if((*count)*a->blk_size > a->tot_size-a->blk_size-1){
	if(lseek(a->fd,0,SEEK_SET)<0)
	  error("error seeking to front of file");
      }	
    }
    
    //DPRINTF(("writing block %d:\n",*count));
    n = a->blk_size;
    while(n){//make sure we write the full ammount
      nn = write(a->fd,src+a->blk_size-n,n);
      //DPRINTF(("\t wrote %d bytes\n",nn));
      if(nn<0)
	error("error writing to file");
      n -=nn;
    }
    (*count)++;
  }
  DPRINTF(("%d writes\n",*count));
  free(src);
  fsync(a->fd);
  pthread_exit((void*)count);
  return NULL;
}


void *ram_read(void *args){
  DPRINTF(("ram read\n"));
  worker_args * a = (worker_args *)args;  
  int64_t offset = -a->blk_size;
  void *dst = malloc(a->blk_size); 

  uint64_t* count=malloc(sizeof(uint64_t));
  *count = 0;

  time_t start = time(NULL);
  while(time(NULL)-start < a->duration){    
    if(a->pattern){/*for random access, first choose a random location and seek to it*/
      double per = rand()*1.0/RAND_MAX;
      offset = (off_t)(per*1.0*(a->tot_size-a->blk_size-1));
    }else{/*sequential*/
      offset+=a->blk_size;
      if(offset > a->tot_size-a->blk_size-1)
	offset = 0;
    }
    //DPRINTF(("reading block %d:\n",*count));
    memcpy(dst,a->buf+offset,a->blk_size);
    (*count)++;
  }
  DPRINTF(("%d reads\n",*count));
  free(dst);
  pthread_exit((void*)count);
  return NULL;
}


void *ram_write(void *args){
  DPRINTF(("ram write\n"));
  worker_args * a = (worker_args *)args;
  int64_t offset = -a->blk_size;

  uint64_t* count=malloc(sizeof(uint64_t));
  *count = 0;

  time_t start = time(NULL);
  while(time(NULL)-start < a->duration){
    /*for random access, first choose a random location*/
    if(a->pattern){
      double per = rand()*1.0/RAND_MAX;
      offset = (off_t)(per*1.0*(a->tot_size-a->blk_size-1));
    }else{/*sequential*/
      offset+=a->blk_size;
      if(offset > a->tot_size-a->blk_size-1)
	offset = 0;
    }
      
    //DPRINTF(("writing block %d:\n",*count));
    memset(a->buf+offset,'x',a->blk_size);
    (*count)++;
  }
  DPRINTF(("%d writes\n",*count));
  pthread_exit((void*)count);
  return NULL;
}


void error(char * msg){
  perror(msg);
  exit(0);
}

void print_bw_latency(time_t duration,long total_requests,long blk_size){

  //calculate throughput
  double bw = (total_requests*blk_size/1000000.0)/duration;
  printf("%g\n",bw);

  //calculate latency
  double lat = (duration)*1000.0/(1.0*total_requests);
  printf("%g\n",lat);
}
