#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define DEBUG 0

#if DEBUG == 1
  #define DPRINTF(args) printf args
#else
  #define DPRINTF(args) 
#endif

/*
  TODO: drink more coffee,shower
 */

/*
  USAGE: $ netmark-cli SIZE SECONDS NUMTHREADS PROTOCOL SERVIP SERVPORT 
  
  SIZE     - number of bytes to send each request
  SECONDS  - number of seconds each thread will run for
  NUMTHREADS  - number of clients to create, each its own thread
  PROTOCOL - 0 for tcp, or 1 for udp
  SERVIP   - ip of server ex. "192.168.0.1"
  SERVPORT - port the server is listening on "1234"

  OUTPUT:
  bandwidth(Mb/s)
  latency(ms/request)
*/

typedef struct {
  int fd;         /*socket file descriptor*/
  int size;       /*size of each request*/
  int duration;   /*duration the thread will send for*/
  char *buf;      /*send/recv buf of length .size*/
  struct in_addr serv_ip; /*used for udp sockets*/
} worker_args;

void error(char * meg);
void * tcp_worker(void * a);
void * udp_worker(void * a);


int main(int argc, char** argv){
  DPRINTF(("starting up...\n"));
  int size = atoi(argv[1]);
  int duration = atoi(argv[2]);
  int num_threads = atoi(argv[3]);
  int protocol    = atoi(argv[4]);

  

  DPRINTF(("sending %db requests for %d seconds with %d threads\n",size,duration,num_threads));

  struct sockaddr_in serv_addr;
  memset((void*)&serv_addr,0,sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  //convert ip address from string to network byte order
  inet_pton(AF_INET,argv[5],&(serv_addr.sin_addr));  
  //convert port from string to network byte order
  serv_addr.sin_port = htons((uint16_t)atoi(argv[6]));

  //this is what is sent each requests
  char buf[size];

  //create a socket for each thread
  DPRINTF(("creating sockets...\n"));
  worker_args args[num_threads];
  for(int i=0;i<num_threads;i++){
    args[i].fd = socket(AF_INET,SOCK_STREAM,0);
    if(args[i].fd < 0)
      error("error creating socket");
  
    if (connect(args[i].fd,(struct sockaddr*)&serv_addr,sizeof(struct sockaddr_in)) < 0)
      error("error connection to server");
    //tell the server the protocol and size of each request
    char info[8];
    *(int32_t*)info = (int32_t)protocol;
    *(int32_t*)(info+4) = (int32_t)size;
    if(write(args[i].fd,info,8)<8)//lets hope it all goes through
      error("error sending protocol and size to server");
    
    args[i].size = size;
    args[i].duration = duration;
    args[i].buf = buf;    
    args[i].serv_ip = serv_addr.sin_addr;
  }

  time_t start = time(NULL);
  
  //spin up a thread for each connection
  DPRINTF(("starting threads...\n"));
  pthread_t thread[num_threads];
  for(int i=0;i<num_threads;i++){
    pthread_create(&thread[i],NULL,(protocol)?(udp_worker):(tcp_worker),(void*)&args[i]);
  }

  //wait for all threads to finish their work
  unsigned long num_requests=0;
  uint64_t *ct;
  for(int i=0;i<num_threads;i++){
    pthread_join(thread[i],(void**)&ct);
    num_requests += *ct;
    free(ct);
  }

  time_t finish = time(NULL);
  //calculate throughput
  double bw = (8.0*size*num_requests/num_threads/1000000)/(finish-start);
  printf("%g\n",bw);

  //calculate latency
  double lat = (finish-start)*1000.0/num_requests;
  printf("%g\n",lat);
}


void * tcp_worker(void * args){ 
  worker_args * a = (worker_args *)args;
  int n,nn;
  char resp;
  uint64_t* i=malloc(sizeof(uint64_t));
  DPRINTF(("Thread started, sending for %d seconds\n",a->duration));
  *i = 0;
  time_t start = time(NULL);
  //send all the requests
  while(time(NULL)-start < a->duration){
    DPRINTF(("sending request %d:\n",*i));
    n = a->size;
    while(n){//make sure we write the full ammount
      DPRINTF(("\t attempting to write %d bytes to %d \n",n,a->fd));
      nn = write(a->fd,a->buf+a->size-n,n);
      DPRINTF(("\t wrote %d bytes\n",nn));
      if(nn<0)
	error("error writing to socket");
      n -=nn;
    }

    //wait for response
    DPRINTF(("\t waiting for response\n"));
    n = read(a->fd,&resp,1);
    if(n<0)
      error("error reading from socket");
    (*i)++;
  }
  if(close(a->fd)<0)
    error("error closing socket");
  pthread_exit((void*)i);
  return 0;
}


void *udp_worker(void *args){
  worker_args * a = (worker_args *)args;
  struct sockaddr_in server_addr;
  uint64_t* i=malloc(sizeof(uint64_t));
  *i = 1;
  DPRINTF(("waiting for new port to reconnect from server\n"));
  //read the udp port to reconnect to from server
  /*just assume we read it all,fix if necessary*/
  if(read(a->fd,(void*)&server_addr,sizeof(server_addr))!=sizeof(server_addr))
    error("error reading upd connection info from server");

  DPRINTF(("got the new port its %d\n",server_addr.sin_port));
  server_addr.sin_addr = a->serv_ip;
  
  //close tcp socket
  if(close(a->fd)<0)
    error("error closing tcp socket");

  DPRINTF(("establishing udp connection...\n"));
  //connect to the udp port
  int cfd;
  if((cfd = socket(AF_INET,SOCK_DGRAM,0))<0)
    error("error creating udp socket");

  if(connect(cfd,(struct sockaddr*)&server_addr,sizeof(server_addr))<0)
    error("error connecting to udp server");
  
  time_t start = time(NULL);
  DPRINTF(("Beginning request/response loop\n"));
  //request-response loop
  uint32_t n; /*num confirmed packets*/
  while(time(NULL)-start < a->duration){
    //write packet to server
    DPRINTF(("write a packet\n"));
    if(send(cfd,a->buf,a->size,0)!=a->size)
      error("error sending udp packet to server");
    
    //check for response
    DPRINTF(("check for response..."));
    if(recv(cfd,&n,sizeof(n),MSG_DONTWAIT)<0){
      if(errno != EAGAIN && errno != EWOULDBLOCK)
	error("error receiving from udp socket");	
      DPRINTF(("nothing\n"));
    }else{
      DPRINTF(("confirmed %d\n",n));
    }    
  }
  
  DPRINTF(("time up\n"));
  *i = n;
  
  if(close(cfd)<0)
    error("error closing udp socket");
  pthread_exit((void*)i);
  return NULL;
}


void error(char * msg){
  perror(msg);
  exit(0);
}

